# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 14:07:14 2020

@author: SK
"""

import itertools as it
import random


RANKS = ['A', 'K', 'Q', 'J', '10', '9', '8', '7', '6', '5', '4', '3', '2', '1']
SUITS = ['H', 'D', 'C', 'S'] # Hearts, Diamonds, Clubs, Stars


def create_deck():
    '''return generator'''
    for rank in RANKS:
        for suit in SUITS:
            yield rank, suit

cards = create_deck()
# cards = it.product(RANKS, SUITS) # itertools version

'''
1.	Shuffle cards in the deck: randomly mix the cards in the card deck, 
and return a whole deck of cards with a mixed order 
'''

def shuffle(deck):
    deck = list(deck)
    random.shuffle(deck)
    return iter(tuple(deck))

cards = shuffle(cards)



'''
2.	Get a card from the top of the deck: get one card from top of 
the card deck, return a card, and if there is no card left in the deck 
return error or exception.  
'''
def get_one_card_from_top():
    try:
        return (list(next(cards)))
    except StopIteration:
        return 'end of deck'



'''
3.	Sort cards: take a list of color as parameter and sort the card in that 
color order. 
Numbers should be in ascending order.  

i.e. If the deck has a card contains with following order  
    (red, 1), (green, 5), (red, 0), (yellow, 3), (green, 2) 

Sort cards([yellow, green, red]) will return the cards with following order 
    (yellow, 3), (green, 0), (green, 5), (red, 0), (red, 1)
'''    

deck = [('red', 1), ('green', 5), ('red', 0), ('yellow', 3), ('green', 2)]
order_list = ['yellow', 'green', 'red']

def sort_by_color(order_list, deck):
    for i in range(len(deck)):  
        for j in range(len(deck)-i-1):
            if order_list.index(deck[j][0]) > order_list.index(deck[j + 1][0]):  
                temp = deck[j]  
                deck[j]= deck[j + 1]  
                deck[j + 1]= temp  
    return deck



#sort_by_color(['yellow', 'green', 'red'], deck)
#sort_by_color(['green', 'red', 'yellow'], deck)





'''
4.	Determine winners: 2 players play the game. They will draw 3 cards by 
taking turns. 
Whoever has the high score wins the game. 
(color point calculation, red = 3, yellow =2, green = 1) 
the point is calculated by color point * number in the card.   
'''
