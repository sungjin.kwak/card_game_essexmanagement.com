# -*- coding: utf-8 -*-
"""
Created on Wed Apr  2 13:22:22 2020

@author: SK
"""

import pytest
from card.project_cards import RANKS, SUITS, create_deck, shuffle, get_one_card_from_top, sort_by_color




def test_shuffle():
    deck1 = create_deck()
    deck2 = create_deck()
    shuffle1 = list(shuffle(deck1))
    shuffle2 = list(shuffle(deck2))
    
    num_of_match = 0
    for i in range(len(shuffle1)):
        if shuffle1[i] == shuffle2[i]:
            num_of_match += 1
    
    percent = num_of_match / len(shuffle1)
    
    print(f'{percent}% identical')    
    
    assert percent < 100, 'The decks are 100% identical, shuffle test fail'


def test_get_one_card_from_top():
    while True:
        get_one_card_from_top()
        if get_one_card_from_top() == 'end of deck':
            print('end of deck')
            break



def test_sort_by_color(order_list = ['yellow', 'green', 'red']):
    deck = [('red', 1), ('green', 5), ('red', 0), ('yellow', 3), ('green', 2)]
    assert sort_by_color(order_list, deck) != list(set(deck)), 'different order, failed test'